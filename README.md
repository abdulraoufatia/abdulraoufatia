### Hey there <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px">
<a href="https://discord.gg/M9VSPbDSx8">
  <img align="left" alt="Abdulraouf's Discord" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/discord.svg" />
</a>
<a href="https://www.linkedin.com/in/abdulraoufatia">
  <img align="left" alt="Abdulraouf's LinkdeIN" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />
</a>

<br />

A trainee biomedical scientist and passionate student developer; ML & AI 🚀 Beside's programming, I enjoy watersports and traveling. I hope like to combine both Biomedical Science and AI Engineering to make my community a better place. 

  <img align="right" alt="GIF" src="https://github.com/abhisheknaiidu/abhisheknaiidu/blob/master/code.gif?raw=true" width="500" height="320" />
  
**A little about me:**

- 👨🏽‍💻 I’m currently working on something cool :wink:;
- 🌱 I’m currently learning Python and MATLAB; 
- 💬 Ask me about anything, I am happy to help;
- 📫 How to reach me: [@Abdulraouf Atia](https://www.linkedin.com/in/abdulraoufatia/);
- 📝[Resume](its incoming)

**Languages and Tools:**  

<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/mysql/mysql.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png"></code>
 
<img src="https://media.giphy.com/media/LnQjpWaON8nhr21vNW/giphy.gif" width="60"> <em><b>I love connecting with different people</b> so if you want to say <b>hi, I'll be happy to meet you more!</b> :)</em>  
